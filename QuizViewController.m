//
//  QuizViewController.m
//  Quiz
//
//  Created by Jay Liew on 5/29/15.
//  Copyright (c) 2015 Jay Liew. All rights reserved.
//

#import "QuizViewController.h"

@interface QuizViewController()

@property (nonatomic) int currentQuestionIndex;
@property (nonatomic, copy) NSArray *questions;
@property (nonatomic, copy) NSArray *answers;

@property (nonatomic, weak) IBOutlet UILabel *questionLabel;
@property (nonatomic, weak) IBOutlet UILabel *answerLabel;

@end

@implementation QuizViewController


- (instancetype)initWithNibName:(NSString *)nibNameOrNil
                         bundle:(NSBundle *)nibBundleOrNil
{
    // Call the init method implemented by the superclass
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self){
        // Create two arrays filled with questions and answers
        // and make pointers to them
        self.questions = @[@"CO 1SG?",
                           @"Commander-in-chief?",
                           @"Secretary of Defense?",
                           @"Secretary of Army?",
                           @"Army Chief of Staff?",
                           @"Sergeant Major of Army?",
                           @"TRADOC CMDR?",
                           @"TRADOC CSM?",
                           @"IMT DCG?",
                           @"IMT CSM?",
                           @"Cyber & POST CMDR?",
                           @"Cyber & POST CSM?",
                           @"BDE CMDR?",
                           @"BDE CSM?",
                           @"BN CMDR?",
                           @"BN CSM?",
                           @"CO CMDR?",
                           @"CO XO"
                           ];
        
        self.answers = @[@"1SG Jenkins",
                         @"Pres. Barack Obama",
                         @"Hon. Ashton Carter",
                         @"Hon. John McHugh",
                         @"GEN Odierno",
                         @"SMA Dailey",
                         @"GEN Perkins",
                         @"CSM Davenport",
                         @"MG Ridge",
                         @"CSM Woods",
                         @"MG Fogarty",
                         @"CSM Cherry",
                         @"COL Reese",
                         @"CSM Williams",
                         @"LTC Comello",
                         @"CSM Kamakahi-Watson",
                         @"CPT Pease",
                         @"1LT Chopp"
                         ];
        
    }
    
    return self;
    
}


- (IBAction)showQuestion:(UIButton *)sender
{
    self.currentQuestionIndex++;
    
    if (self.currentQuestionIndex == [self.questions count])
    {
        self.currentQuestionIndex = 0;
    }
    
    NSString *question = self.questions[self.currentQuestionIndex];
    
    self.questionLabel.text = question;
    
    self.answerLabel.text = @"???";
    
}

- (IBAction)showAnswer:(UIButton *)sender
{
    NSString *answer = self.answers[self.currentQuestionIndex];
    
    self.answerLabel.text = answer;
}

/*
 - (void)viewDidLoad {
 [super viewDidLoad];
 // Do any additional setup after loading the view from its nib.
 }
*/


/*
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
*/
 
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
