//
//  AppDelegate.h
//  Quiz
//
//  Created by Jay Liew on 5/29/15.
//  Copyright (c) 2015 Jay Liew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

