//
//  main.m
//  Quiz
//
//  Created by Jay Liew on 5/29/15.
//  Copyright (c) 2015 Jay Liew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
